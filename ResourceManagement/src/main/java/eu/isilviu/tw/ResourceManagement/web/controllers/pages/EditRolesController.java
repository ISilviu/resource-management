package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.services.RoleRestService;
import eu.isilviu.tw.ResourceManagement.services.UserRestService;
import eu.isilviu.tw.ResourceManagement.utils.security.LoginMiddleware;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class EditRolesController {

    @Autowired
    private LoginMiddleware loginMiddleware;

    @Autowired
    private UserRestService userRestService;

    @Autowired
    private RoleRestService roleRestService;

    @GetMapping(value = "/user/{username}/roles/edit")
    public String editRoles(Model model, HttpServletRequest request, @PathVariable("username") String username) throws IOException {
        if(loginMiddleware.ensureAdminLoggedIn(request)){
            var user = userRestService.getUserForUsername(username);
            if(user == null)
                return "redirect:/admin_dashboard";

            model.addAttribute("current_user", user);
            model.addAttribute("user_roles_list", user.getRoles());

            var roles = roleRestService.getRoles();
            model.addAttribute("roles_list", roles);
            return "roles_edit";
        }
        return "redirect:/";
    }

    @GetMapping(value = "/user/{username}/roles/{roleTitle}/add")
    public String addRole(Model model, HttpServletRequest request,
                          @PathVariable("username") String username,
                          @PathVariable("roleTitle") String roleTitle) throws IOException {
        if (loginMiddleware.ensureAdminLoggedIn(request)) {
            var user = userRestService.getUserForUsername(username);
            var role = roleRestService.getRoleByTitle(roleTitle);
            if(user != null && role != null){
                user.addRole(role);
                userRestService.updateUser(user);
            }
        }
        String forwardPath  =  "forward:/user/" + username + "/roles/edit";
        return forwardPath;
    }

    @GetMapping(value = "/user/{username}/roles/{roleTitle}/remove")
    public String removeRole(Model model, HttpServletRequest request,
                          @PathVariable("username") String username,
                          @PathVariable("roleTitle") String roleTitle) throws IOException {
        if (loginMiddleware.ensureAdminLoggedIn(request)) {
            var user = userRestService.getUserForUsername(username);
            var role = roleRestService.getRoleByTitle(roleTitle);
            if(user != null && role != null){
                user.removeRole(role);
                userRestService.updateUser(user);
            }
        }
        String forwardPath  =  "forward:/user/" + username + "/roles/edit";
        return forwardPath;
    }
}
