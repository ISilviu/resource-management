package eu.isilviu.tw.ResourceManagement.persistence;


import eu.isilviu.tw.ResourceManagement.models.Resource;
import eu.isilviu.tw.ResourceManagement.models.Right;
import org.springframework.data.jpa.repository.JpaRepository;
import eu.isilviu.tw.ResourceManagement.models.RightType;
import org.springframework.stereotype.Repository;


import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface RightsRepository extends JpaRepository<Right, Long> {

    boolean existsByRightType(@NotNull RightType rightType);

    List<Right> findAllByResource(Resource resource);

    List<Right> findAllByRoleId(@NotNull Long role_id);
}
