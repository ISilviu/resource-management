package eu.isilviu.tw.ResourceManagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Role")
@Table(name="roles")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "role_id")
    private long id;

    @OneToMany(
            mappedBy = "role",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference
    private List<Right> rights = new ArrayList<>();

    @ManyToMany(mappedBy = "roles")
    private List<User> users = new ArrayList<>();

    @NotNull
    @Column(unique = true)
    @NaturalId
    private String title;

    public Role() { }

    public Role(String title){
        this.title = title;
    }

    public Role(String title, List<Right> rights){
        this.title = title;
        this.rights = rights;
    }

    public long getId() { return id; }

    public List<Right> getRights() { return rights; }

    public String getTitle() { return title; }
}
