package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.models.RightType;
import eu.isilviu.tw.ResourceManagement.services.ResourceRestService;
import eu.isilviu.tw.ResourceManagement.services.RoleRestService;
import eu.isilviu.tw.ResourceManagement.services.UserRestService;
import eu.isilviu.tw.ResourceManagement.utils.permissions.UserPermissionsProvider;
import eu.isilviu.tw.ResourceManagement.utils.security.LoginMiddleware;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.RightDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Controller
public class DashboardController {

    @Autowired
    private UserRestService userRestService;

    @Autowired
    private RoleRestService roleRestService;

    @Autowired
    private ResourceRestService resourceRestService;

    @Autowired
    private LoginMiddleware loginMiddleware;

    @Autowired
    private UserPermissionsProvider userPermissionsProvider;

    private static final Map<RightDTO, String> STUDENT_RIGHT_TO_PAGE = new HashMap<>();
    static {
        STUDENT_RIGHT_TO_PAGE.put(new RightDTO(RightType.CREATE), "create_student");
        STUDENT_RIGHT_TO_PAGE.put(new RightDTO(RightType.CREATE), "read_students");
    }

    @GetMapping("/admin_dashboard")
    public String adminDashboard(Model model, HttpServletRequest request) throws IOException {
        if(loginMiddleware.ensureAdminLoggedIn(request)){
            var currentUser = (UserDTO)request.getSession().getAttribute("current_user");
            model.addAttribute("current_user", currentUser);

            var users = userRestService.getAllUsersForAdmin(false);
            model.addAttribute("users_list", users);

            var roles = roleRestService.getRoles();
            model.addAttribute("roles_list", roles);

            return "admin_dashboard";
        }
        return "redirect:/";
    }

    @GetMapping("/user_dashboard")
    public String userDashboard(Model model, HttpServletRequest request) throws IOException {
        if(loginMiddleware.ensureLoggedIn(request)){
            var currentUser = (UserDTO)request.getSession().getAttribute("current_user");
            model.addAttribute("current_user", currentUser);

            var resources = resourceRestService.getResources();
            model.addAttribute("resources_list", resources);

            return "user_dashboard";
        }
        return "redirect:/";
    }

//    @GetMapping("/resources/{tableName}/{rightType}")
//    public String createPage(Model model,
//                         HttpServletRequest request,
//                         @PathVariable("tableName") String tableName,
//                         @PathVariable("rightType") String rightTypeString) throws IOException {
//        if (loginMiddleware.ensureLoggedIn(request)) {
//            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
//            var currentUserEntity = userRestService.getUserForId(currentUser.getId());
//            if (currentUserEntity == null)
//                return "redirect:/user_dashboard";
//
//            RightDTO requiredRight = new RightDTO(RightType.valueOf(rightTypeString.toUpperCase()));
//            ResourceDTO requiredResource = new ResourceDTO(tableName);
//            boolean hasRight = userPermissionsProvider
//                    .fetchNew(currentUser.getId())
//                    .hasRight(requiredRight, requiredResource);
//            if (!hasRight) {
//                model.addAttribute("error_message", "You don't have the right to do this action!");
//                return "forward:/user_dashboard";
//            } else {
//                return String.format("redirect:/%s/%s", tableName, rightTypeString + "-page");
//            }
//        }
//        return "redirect:/";
//    }
//
//    private String getRequiredPage(Model model, HttpServletRequest request, RightDTO intent) throws IOException {
//        if(loginMiddleware.ensureLoggedIn(request)){
//            model.addAttribute("tableName", "Students");
//            model.addAttribute("rightType", "create");
//            switch (intent.getRightType()){
//                case READ: return "redirect:/";
//                case CREATE: return "create_student";
//            }
//        }
//        return "redirect:/";
//    }

//    @GetMapping("/resources/Students/create")
//    public String create(Model model,
//                       HttpServletRequest request) throws  IOException{
//        return getRequiredPage(model, request, new RightDTO(RightType.CREATE));
//    }

}
