package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.models.RightType;
import eu.isilviu.tw.ResourceManagement.services.ResourceRestService;
import eu.isilviu.tw.ResourceManagement.services.RightRestService;
import eu.isilviu.tw.ResourceManagement.services.RoleRestService;
import eu.isilviu.tw.ResourceManagement.utils.security.LoginMiddleware;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.RightDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class NewRoleController {

    @Autowired
    private RightRestService rightRestService;

    @Autowired
    private RoleRestService roleRestService;

    @Autowired
    private ResourceRestService resourceRestService;

    private static final List<String> CRUD_OPERATIONS = new ArrayList<>();
    static {
        CRUD_OPERATIONS.add("create");
        CRUD_OPERATIONS.add("read");
        CRUD_OPERATIONS.add("update");
        CRUD_OPERATIONS.add("delete");
    }

    private final List<String> TABLE_NAMES = new ArrayList<>();

    private final Map<String, List<String>> RESOURCE_NAME_TO_RIGHTS = new HashMap<>();

    private List<ResourceDTO> resources;

    private Pattern operationTypePattern = Pattern.compile("^[a-zA-Z]+_([a-z]+)$");

    @Autowired
    LoginMiddleware loginMiddleware;

    @GetMapping("/role/new")
    public String main(Model model, HttpServletRequest request) throws IOException {
        if(loginMiddleware.ensureAdminLoggedIn(request)){
            resources = resourceRestService.getResources();
            Runnable runnable = () -> {
                resources.forEach(resource -> {
                    List<String> operations = new ArrayList<>(CRUD_OPERATIONS.size());
                    CRUD_OPERATIONS.forEach(operation ->{
                        operations.add(resource.getTableName() + "_" + operation);
                    });
                    RESOURCE_NAME_TO_RIGHTS.put(resource.getTableName(), operations);
                });
            };
            var thread = new Thread(runnable);
            thread.start();
            model.addAttribute("resources_list", resources);
            return "new_role";
        }
        return "index";
    }

    @PostMapping("/role/new")
    public String newRole(HttpServletRequest request) throws IOException{
        if(!loginMiddleware.ensureAdminLoggedIn(request))
            return "redirect:/";

        var roleName = request.getParameter("role-name");
        if(roleName == null)
            return "redirect:/role/new";

        RoleDTO role = roleRestService.getRoleByTitle(roleName);
        if(role == null){
            role = new RoleDTO(roleName);
            role = roleRestService.createRole(role);
        }

        for(var resource : resources){
            for(var checkBoxName : RESOURCE_NAME_TO_RIGHTS.get(resource.getTableName())){
                if(request.getParameter(checkBoxName) == null)
                    continue;

                Matcher matcher = operationTypePattern.matcher(checkBoxName);
                if(matcher.find()){
                    RightType rightType = RightType.valueOf(matcher.group(1).toUpperCase());
                    RightDTO right = new RightDTO(rightType);
                    right.setResource(resource);
                    right.setRole(role);

                    rightRestService.createRight(right);
                }
            }
        }
        return "redirect:/admin_dashboard";
    }
}
