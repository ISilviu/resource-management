package eu.isilviu.tw.ResourceManagement.web.dto.resources;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class StudentDTO {

    private long id;

    private String name;

    public StudentDTO(){}

    public StudentDTO(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public StudentDTO(String name) {
        this.name = name;
    }

    public String getName() { return name; }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public void setName(String name) { this.name = name; }
}
