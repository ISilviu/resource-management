package eu.isilviu.tw.ResourceManagement.web.dto.resources;

public class HolidayDTO {

    private long id;

    private String title;

    private String description;

    private String country;

    public HolidayDTO(){}

    public HolidayDTO(String title, String description, String country){
        this.country = country;
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }

    public long getId() { return id; }
}
