package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.Role;
import eu.isilviu.tw.ResourceManagement.persistence.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class RolesController {

    @Autowired
    private RolesRepository rolesRepository;

    @GetMapping(
            value = "/api/roles",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @javax.transaction.Transactional
    public List<Role> getRoles(){
        return rolesRepository.findAll();
    }

    @GetMapping(
            value = "/api/roles/{title}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Role getRole(@PathVariable("title") String title){
        Optional<Role> roleOptional = rolesRepository.findByTitle(title);
        return roleOptional.orElse(null);
    }

    @PostMapping(value = "/api/role/create",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces =  MediaType.APPLICATION_JSON_VALUE)
    public Role createRole(@RequestBody Role role) {
        if(!rolesRepository.existsByTitle(role.getTitle()))
            return rolesRepository.save(role);
        return null;
    }

    @PostMapping(value = "/api/role/force/create",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces =  MediaType.APPLICATION_JSON_VALUE)
    public Role forceCreateRole(@RequestBody Role role) {
        if(!rolesRepository.existsByTitle(role.getTitle()))
            return rolesRepository.saveAndFlush(role);
        return null;
    }
}
