package eu.isilviu.tw.ResourceManagement.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.isilviu.tw.ResourceManagement.web.dto.RoleDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Component
public class RoleRestService {

    private RestTemplate restTemplate;

    public RoleRestService(){
        restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        var converter = new MappingJackson2HttpMessageConverter(mapper);
        restTemplate.getMessageConverters().add(converter);
    }

    public List<RoleDTO> getRoles(){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/roles")
                .build()
                .toUriString();
        ResponseEntity<List<RoleDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<RoleDTO>>() {});
        return responseEntity.getBody();
    }

    public RoleDTO createRole(RoleDTO role){
        var requestEntity = new HttpEntity<>(role, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/role/create", HttpMethod.POST, requestEntity, RoleDTO.class)
                .getBody();
    }

    public RoleDTO forceCreateRole(RoleDTO role){
        var requestEntity = new HttpEntity<>(role, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/role/force/create", HttpMethod.POST, requestEntity, RoleDTO.class)
                .getBody();
    }

    public RoleDTO getRoleByTitle(String title){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/roles/" + title)
                .build()
                .toUriString();
        ResponseEntity<RoleDTO> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<RoleDTO>() {});
        return responseEntity.getBody();
    }

    private HttpHeaders createHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
