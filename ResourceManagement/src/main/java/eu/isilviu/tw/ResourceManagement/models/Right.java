package eu.isilviu.tw.ResourceManagement.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity(name = "Right")
@Table(name="rights")
public class Right implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "right_id")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "resource_id")
    private Resource resource;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    @JsonBackReference
    private Role role;

    @NotNull
    @Column(name = "right_type")
    private RightType rightType;

    public Right() { }

    public Right(RightType rightType){ this.rightType = rightType;}

    public Right(RightType rightType, Resource resource) {
        this.rightType = rightType;
        this.resource = resource;
    }

    public long getId() { return id; }

    public Resource getResource() { return resource; }

    public Role getRole() { return role; }

    public RightType getRightType() { return rightType; }
}
