package eu.isilviu.tw.ResourceManagement.utils.security;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Class containing wrapper methods for BCrypt functionality.
 */
public class HashingUtilities {
    /**
     * Generates a hash for a given input.
     * @param password the password for which to generate the hash
     * @return the hash
     */
    public static String generateHash(String password){
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    /**
     * Generates a has for a given input, and also requires specifying the logRounds.
     * @param password the password
     * @param logRounds the number of log rounds
     * @return the hash
     */
    public static String generateHash(String password, int logRounds){
        return BCrypt.hashpw(password, BCrypt.gensalt(logRounds));
    }

    /**
     * Checks if a given password matches with a hash.
     * @param password the password
     * @param hash the hash
     * @return whether there was a match or not
     */
    public static boolean match(String password, String hash){
        return BCrypt.checkpw(password, hash);
    }
}
