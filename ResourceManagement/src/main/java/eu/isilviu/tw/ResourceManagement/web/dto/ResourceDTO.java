package eu.isilviu.tw.ResourceManagement.web.dto;

import java.util.ArrayList;
import java.util.List;

public class ResourceDTO {

    private long id;

    private List<RightDTO> rights = new ArrayList<>();

    private String tableName;

    public ResourceDTO() { }

    public ResourceDTO(String tableName, List<RightDTO> rights){
        this.tableName = tableName;
        this.rights = rights;
    }

    public ResourceDTO(String tableName){
        this.tableName = tableName;
    }

    public long getId() { return id; }

    public List<RightDTO> getRights() { return rights; }

    public String getTableName() { return tableName; }

    public void setRights(List<RightDTO> rights) { this.rights = rights; }

    public void setTableName(String tableName) { this.tableName = tableName; }
}
