package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.models.User;
import eu.isilviu.tw.ResourceManagement.persistence.UsersRepository;
import eu.isilviu.tw.ResourceManagement.services.UserRestService;
import eu.isilviu.tw.ResourceManagement.utils.security.LoginMiddleware;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class IndexController {

    @Autowired
    private LoginMiddleware loginMiddleware;

    @Autowired
    private UserRestService userRestService;

    @GetMapping("/")
    public String main(HttpServletRequest request){
x` `        if (loginMiddleware.isLoggedIn(request)) {
            var user = ((UserDTO) request.getSession().getAttribute("current_user"));
            if(user.isAdmin() == null || !user.isAdmin())
                return "redirect:/user_dashboard";
            return "redirect:/admin_dashboard";
        }
        return "index";
    }

    @PostMapping("/")
    public String index(Model model){
        return "index";
    }

    @PostMapping("/login")
    public String login(Model model,
                        HttpServletRequest request,
                        HttpServletResponse response,
                        @RequestParam("username") String username,
                        @RequestParam("password") String password) throws IOException, ServletException {
        if (loginMiddleware.login(username, password)) {
            UserDTO user = userRestService.getUserForUsername(username);
            request.getSession().setAttribute("current_user", user);
            if(user.isAdmin() == null || !user.isAdmin())
                return "redirect:/user_dashboard";
            return "redirect:/admin_dashboard";
        }
        request.setAttribute("login_failed_message", "The username and the password didn't match.");
        return "forward:/";
    }
}
