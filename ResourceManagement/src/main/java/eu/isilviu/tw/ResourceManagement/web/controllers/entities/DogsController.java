package eu.isilviu.tw.ResourceManagement.web.controllers.entities;

import eu.isilviu.tw.ResourceManagement.models.User;
import eu.isilviu.tw.ResourceManagement.persistence.UsersRepository;
import eu.isilviu.tw.ResourceManagement.utils.security.HashingUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DogsController {

    @Autowired
    private UsersRepository usersRepository;

    @PostMapping("/users/add")
    public String register(Model model,
                           @RequestParam("username")   String username,
                           @RequestParam("first-name") String firstName,
                           @RequestParam("last-name")  String lastName,
                           @RequestParam("password")   String password){
        if(usersRepository.getByUsername(username) != null){
            model.addAttribute("success", false);
            model.addAttribute("message", "There already exists an user with that username.");
            return "register";
        }
        var user = new User(username, firstName, lastName, HashingUtilities.generateHash(password), false);
        usersRepository.save(user);
        model.addAttribute("success", true);
        return "register";
    }
}
