package eu.isilviu.tw.ResourceManagement.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class ResourceRestService {

    private RestTemplate restTemplate;

    public ResourceRestService(){
        restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        var converter = new MappingJackson2HttpMessageConverter(mapper);
        restTemplate.getMessageConverters().add(converter);
    }

    public List<ResourceDTO> getResources(){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/resources")
                .build()
                .toUriString();
        ResponseEntity<List<ResourceDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {
                });
        return responseEntity.getBody();
    }
}
