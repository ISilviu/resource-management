package eu.isilviu.tw.ResourceManagement.models.resources;

import eu.isilviu.tw.ResourceManagement.models.Role;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Holiday")
@Table(name="holidays")
public class Holiday implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "holiday_id")
    private long id;

    @NotNull
    @Column(unique = true)
    private String title;

    @NotNull
    @Column
    private String description;

    @NotNull
    @Column
    private String country;

    public Holiday() { }

    public Holiday(String title, String description, String country){
        this.country = country;
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }
}