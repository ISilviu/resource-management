package eu.isilviu.tw.ResourceManagement.models;

public enum RightType {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
