package eu.isilviu.tw.ResourceManagement.models.resources;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Student")
@Table(name="students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "student_id")
    private long id;

    @NotNull
    @Column
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public Student(){ }

    public Student(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() { return name; }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public void setName(String name) { this.name = name; }
}
