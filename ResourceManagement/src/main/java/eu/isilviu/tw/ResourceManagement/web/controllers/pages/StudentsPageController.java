package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.models.RightType;
import eu.isilviu.tw.ResourceManagement.services.StudentRestService;
import eu.isilviu.tw.ResourceManagement.utils.permissions.UserPermissionsProvider;
import eu.isilviu.tw.ResourceManagement.utils.security.LoginMiddleware;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.RightDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.resources.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class StudentsPageController {

    @Autowired
    private UserPermissionsProvider userPermissionsProvider;

    @Autowired
    private LoginMiddleware loginMiddleware;

    @Autowired
    private StudentRestService studentRestService;

    private static final ResourceDTO STUDENTS_REQUIRED_RESOURCE = new ResourceDTO("Students");

    @PostMapping("/resources/Students/create")
    public String create( Model model,
                          HttpServletRequest request,
                          @RequestParam("name") String name) throws IOException {
        if(loginMiddleware.ensureLoggedIn(request)){
            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");

            StudentDTO student = new StudentDTO(name);
            RightDTO requiredRight = new RightDTO(RightType.CREATE);
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, STUDENTS_REQUIRED_RESOURCE);
            if(hasRight){
               studentRestService.createStudent(student);
               return "redirect:/user_dashboard";
            } else{
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }
        return "redirect:/";
    }

    @GetMapping("/resources/Students/create")
    public String create(Model model,
                         HttpServletRequest request) throws IOException{
        if(loginMiddleware.ensureLoggedIn(request)){{
            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
            RightDTO requiredRight = new RightDTO(RightType.CREATE);
            ResourceDTO requiredResource = new ResourceDTO("Students");
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, requiredResource);

            if(hasRight){
                model.addAttribute("tableName", "Students");
                model.addAttribute("rightType", "create");
                return "create_student";
            } else{
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }}
        return "redirect:/";
    }

    @GetMapping("/resources/Students/read")
    public String read(Model model,
                         HttpServletRequest request
                         ) throws IOException {
        if(loginMiddleware.ensureLoggedIn(request)){

            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
            RightDTO requiredRight = new RightDTO(RightType.READ);
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, STUDENTS_REQUIRED_RESOURCE);
            if(hasRight){
                var students = studentRestService.getStudents();
                model.addAttribute("students_list", students);
                return "read_students";
            } else {
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }
        return  "redirect:/";
    }

    @GetMapping("/resources/Students/update/{studentId}")
    public String update(Model model,
                         HttpServletRequest request,
                         @PathVariable("studentId") Long id
    ) throws IOException{
        if(loginMiddleware.ensureLoggedIn(request)){
            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
            RightDTO requiredRight = new RightDTO(RightType.UPDATE);
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, STUDENTS_REQUIRED_RESOURCE);
            if(hasRight){
                model.addAttribute("student_id",  id);
                return "update_student";
            } else {
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }
        return "redirect:/";
    }

    @GetMapping("/resources/Students/delete/{studentId}")
    public String delete(Model model,
                         HttpServletRequest request,
                         @PathVariable("studentId") Long id
    ) throws IOException{
        if(loginMiddleware.ensureLoggedIn(request)){
            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
            RightDTO requiredRight = new RightDTO(RightType.DELETE);
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, STUDENTS_REQUIRED_RESOURCE);
            StudentDTO student = studentRestService.getById(id);
            if(student == null){
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
            if(hasRight){
                studentRestService.deleteStudent(student);
                return "redirect:/user_dashboard";
            } else {
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/resources/Students/update/{studentId}")
    public String updateStudent(Model model,
                                HttpServletRequest request,
                                @PathVariable("studentId") Long id,
                                @RequestParam("name") String name
    ) throws IOException{
        if(loginMiddleware.ensureLoggedIn(request)){
            var currentUser = (UserDTO) request.getSession().getAttribute("current_user");
            RightDTO requiredRight = new RightDTO(RightType.UPDATE);
            StudentDTO student = studentRestService.getById(id);
            if(student == null){
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
            boolean hasRight = userPermissionsProvider
                    .fetchNew(currentUser.getId())
                    .hasRight(requiredRight, STUDENTS_REQUIRED_RESOURCE);
            if(hasRight){
                student.setName(name);
                studentRestService.updateStudent(student);
                return "redirect:/user_dashboard";
            } else {
                model.addAttribute("error_message", "You don't have the right to access this.");
                return "forward:/user_dashboard";
            }
        }
        return "redirect:/";
    }


}
