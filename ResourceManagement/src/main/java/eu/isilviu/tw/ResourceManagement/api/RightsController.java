package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.Right;
import eu.isilviu.tw.ResourceManagement.persistence.RightsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
public class RightsController {

    @Autowired
    private RightsRepository rolesRepository;

    @GetMapping(
            value = "/api/rights",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Right> getRights(){
        return rolesRepository.findAll();
    }

    @GetMapping(
            value = "/api/right/for",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Right> getRightsForRole(@RequestParam(name = "roleId", required = true) Long roleId){
        return rolesRepository.findAllByRoleId(roleId);
    }

    @PostMapping(value = "/api/right/create",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces =  MediaType.APPLICATION_JSON_VALUE)
    public Right createRight(@RequestBody Right role) {
        return rolesRepository.save(role);
    }
}
