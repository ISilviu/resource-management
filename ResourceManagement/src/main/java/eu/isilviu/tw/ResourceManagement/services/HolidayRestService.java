package eu.isilviu.tw.ResourceManagement.services;

import eu.isilviu.tw.ResourceManagement.web.dto.resources.HolidayDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.resources.StudentDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Component
public class HolidayRestService {

    private RestTemplate restTemplate;

    public HolidayRestService(){
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    public HolidayDTO createHoliday(HolidayDTO holiday){
        var requestEntity = new HttpEntity<>(holiday, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/holiday/create", HttpMethod.POST, requestEntity, HolidayDTO.class)
                .getBody();
    }

    public HolidayDTO updateHoliday(HolidayDTO holiday){
        var requestEntity = new HttpEntity<>(holiday, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/holiday/update", HttpMethod.PUT, requestEntity, HolidayDTO.class)
                .getBody();
    }

    public List<HolidayDTO> getHolidays(){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/holidays")
                .build()
                .toUriString();
        ResponseEntity<List<HolidayDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {
                });
        return responseEntity.getBody();
    }

    public void deletHoliday(HolidayDTO holiday){
        var requestEntity = new HttpEntity<>(holiday, createHeaders());
        restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/holiday/delete", HttpMethod.DELETE, requestEntity, HolidayDTO.class);
    }

    private HttpHeaders createHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
