package eu.isilviu.tw.ResourceManagement.persistence;

import eu.isilviu.tw.ResourceManagement.models.resources.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HolidaysRepository extends JpaRepository<Holiday, Long> {
}
