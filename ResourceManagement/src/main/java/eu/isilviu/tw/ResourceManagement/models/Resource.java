package eu.isilviu.tw.ResourceManagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Resource")
@Table(name="resources")
public class Resource implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "resource_id")
    private long id;

    @OneToMany(
            mappedBy = "resource",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Right> rights = new ArrayList<>();

    @NotNull
    @Column(name = "table_name", unique = true)
    @NaturalId
    private String tableName;

    public Resource() { }

    public Resource(String tableName, List<Right> rights){
        this.tableName = tableName;
        this.rights = rights;
    }

    public Resource(String tableName){
        this.tableName = tableName;
    }

    public long getId() { return id; }

    public List<Right> getRights() { return rights; }

    public String getTableName() { return tableName; }

    public void setTableName(String tableName) { this.tableName = tableName; }
}
