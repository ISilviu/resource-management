package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.resources.Holiday;
import eu.isilviu.tw.ResourceManagement.models.resources.Student;
import eu.isilviu.tw.ResourceManagement.persistence.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentsController {

    @Autowired
    private StudentsRepository studentsRepository;

    @PostMapping(value = "/api/student/create",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Student createHoliday(@RequestBody Student holiday){
        return studentsRepository.save(holiday);
    }

    @PutMapping(value = "/api/student/update",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Student updateHoliday(@RequestBody Student student){
        return studentsRepository.save(student);
    }

    @DeleteMapping(value = "/api/student/delete",
            consumes =  MediaType.APPLICATION_JSON_VALUE)
    public void deleteHoliday(@RequestBody Student student){
        if(student == null)
            return;
        studentsRepository.delete(student);
    }

    @GetMapping(value = "/api/students",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Student> getAll(){
        List<Student> students = studentsRepository.findAll();
        return students;
    }

    @GetMapping(value = "/api/student/{studentId}",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public Student getById(@PathVariable("studentId") Long id){
        return studentsRepository.findById(id).orElse(null);
    }
}
