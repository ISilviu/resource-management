package eu.isilviu.tw.ResourceManagement.persistence;

import eu.isilviu.tw.ResourceManagement.models.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourcesRepository extends JpaRepository<Resource, Long> {
}
