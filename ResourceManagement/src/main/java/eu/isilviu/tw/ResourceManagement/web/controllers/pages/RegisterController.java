package eu.isilviu.tw.ResourceManagement.web.controllers.pages;

import eu.isilviu.tw.ResourceManagement.services.UserRestService;
import eu.isilviu.tw.ResourceManagement.utils.security.HashingUtilities;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegisterController {

    @Autowired
    private UserRestService userRestService;

    @GetMapping("/register")
    public String main(Model model){
        return "register";
    }

    @PostMapping("/register")
    public String register(Model model,
                           @RequestParam("username")   String username,
                           @RequestParam("first-name") String firstName,
                           @RequestParam("last-name")  String lastName,
                           @RequestParam("password")   String password){
        UserDTO userDTO = new UserDTO(username, firstName, lastName, HashingUtilities.generateHash(password), false);
        var result = userRestService.createUser(userDTO);
        if(result == null)
            model.addAttribute("message", "There already exists an user with that username.");
        model.addAttribute("success", result != null);
        return "register";
    }
}
