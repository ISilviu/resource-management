package eu.isilviu.tw.ResourceManagement.web.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.ArrayList;
import java.util.List;

public class RoleDTO {

    private long id;

    private List<RightDTO> rights = new ArrayList<>();

    private List<UserDTO> users = new ArrayList<>();

    private String title;

    public RoleDTO() { }

    public RoleDTO(String title){
        this.title = title;
    }

    public RoleDTO(String title, List<RightDTO> rights){
        this.title = title;
        this.rights = rights;
    }

    public long getId() { return id; }

    public List<RightDTO> getRights() { return rights; }

    public String getTitle() { return title; }

}
