package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.User;
import eu.isilviu.tw.ResourceManagement.persistence.UsersRepository;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @GetMapping(
            value = "/api/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getUsers(@RequestParam(name = "isAdmin", required = false) Boolean isAdmin){
        if(isAdmin != null)
            return usersRepository.getAllByIsAdmin(isAdmin);
        return usersRepository.findAll();
    }

    @GetMapping(
            value = "/api/users/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public User getUser(@PathVariable("id") Long id){
        Optional<User> userOptional = usersRepository.findById(id);
        return userOptional.orElse(null);
    }

    @GetMapping(
            value="/api/user",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public User getUserByUsername(@RequestParam(name = "username") String username){
        var user = usersRepository.getByUsername(username);
        return user;
    }

    @PostMapping(value = "/api/user/create",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createUser(@RequestBody User user) {
        if(!usersRepository.existsByUsername(user.getUsername())){
            if(user.isAdmin() == null)
                user.setAdmin(false);

            usersRepository.save(user);
            return ResponseEntity.status(HttpStatus.OK).body("The user was successfully added.");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("There already exists an user with that username dude!");
    }

    @PutMapping(value = "/api/user/update",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public User updateUser(@RequestBody User user){
        if(user.isAdmin() == null)
            user.setAdmin(false);
        return usersRepository.save(user);
    }
}
