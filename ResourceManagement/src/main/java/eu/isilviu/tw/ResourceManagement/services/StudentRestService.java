package eu.isilviu.tw.ResourceManagement.services;

import eu.isilviu.tw.ResourceManagement.web.dto.RoleDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.resources.StudentDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Component
public class StudentRestService {

    private RestTemplate restTemplate;

    public StudentRestService(){
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    public StudentDTO createStudent(StudentDTO student){
        var requestEntity = new HttpEntity<>(student, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/student/create", HttpMethod.POST, requestEntity, StudentDTO.class)
                .getBody();
    }

    public StudentDTO updateStudent(StudentDTO student){
        var requestEntity = new HttpEntity<>(student, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/student/update", HttpMethod.PUT, requestEntity, StudentDTO.class)
                .getBody();
    }

    public List<StudentDTO> getStudents(){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/students")
                .build()
                .toUriString();
        ResponseEntity<List<StudentDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {
                });
        return responseEntity.getBody();
    }

    public StudentDTO getById(Long id){
        String uriString = UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/student/" + id)
                .build()
                .toUriString();
        ResponseEntity<StudentDTO> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {
                });
        return responseEntity.getBody();
    }

    public void deleteStudent(StudentDTO student){
        var requestEntity = new HttpEntity<>(student, createHeaders());
         restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/student/delete", HttpMethod.DELETE, requestEntity, StudentDTO.class);
    }

    private HttpHeaders createHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
