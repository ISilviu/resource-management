package eu.isilviu.tw.ResourceManagement.utils.security;

import eu.isilviu.tw.ResourceManagement.models.User;
import eu.isilviu.tw.ResourceManagement.persistence.UsersRepository;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class LoginMiddleware {
    
    @Autowired
    private UsersRepository USERS_REPOSITORY;

    public LoginMiddleware(){}
    /**
     * Ensures the player is logged in.
     * @param req  the request
     * @return true or false
     * @throws IOException the io exception
     */
    public boolean ensureLoggedIn(HttpServletRequest req) throws IOException {
        var user = getLoggedIn(req);
        return user != null;
    }

    public boolean ensureAdminLoggedIn(HttpServletRequest req) throws IOException{
        var user = getLoggedIn(req);
        if(user == null)
            return false;
        return user.isAdmin() != null && user.isAdmin();
    }

    /**
     * Gets logged in.
     * @param req the request
     * @return the logged in player
     */
    public User getLoggedIn(HttpServletRequest req) {
        var session = req.getSession(true);
        if(session == null)
            return null;

        var user = (UserDTO) req.getSession(true).getAttribute("current_user");
        if(user == null)
            return null;
        var username = user.getUsername();
        return USERS_REPOSITORY.getByUsername(username);
    }

    public boolean login(String username, String password){
        var user = USERS_REPOSITORY.getByUsername(username);
        if (user == null)
            return false;
        return HashingUtilities.match(password, user.getHash());
    }

    /**
     * Finds if a user is logged in.
     * @param req the request
     * @return true or false
     */
    public boolean isLoggedIn(HttpServletRequest req)
    {
        return getLoggedIn(req) != null;
    }
}
