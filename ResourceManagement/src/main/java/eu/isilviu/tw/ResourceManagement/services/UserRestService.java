package eu.isilviu.tw.ResourceManagement.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Component
public class UserRestService {

    private RestTemplate restTemplate;

    public UserRestService(){
        restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        var converter = new MappingJackson2HttpMessageConverter(mapper);
        restTemplate.getMessageConverters().add(converter);
    }

    public List<UserDTO> getAllUsersForAdmin(Boolean isAdmin){
        String uriString =	UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/users")
                .queryParam("isAdmin", isAdmin)
                .build()
                .toUriString();
        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<UserDTO>>() {});
        return responseEntity.getBody();
    }

    public UserDTO getUserForUsername(String username){
        String uriString =	UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/user")
                .queryParam("username", username)
                .build()
                .toUriString();
        ResponseEntity<UserDTO> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {});
        return responseEntity.getBody();
    }

    public UserDTO getUserForId(Long id){
        String uriString =	UriComponentsBuilder.fromHttpUrl(CommonLiterals.HOSTNAME + "/api/users/" + id)
                .build()
                .toUriString();
        ResponseEntity<UserDTO> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {});
        return responseEntity.getBody();
    }
//
//    public List<AppointmentDto> getAllAppointmentsForSelectedPatient(Long patientId){
//        String uriString =	UriComponentsBuilder.fromHttpUrl(HOSTNAME + "/patient/appointments")
//                .queryParam("patientId", patientId)
//                .build()
//                .toUriString();
//        ResponseEntity<List<AppointmentDto>> responseEntity =	restTemplate.exchange(uriString, HttpMethod.GET,
//                null, new ParameterizedTypeReference<List<AppointmentDto>>(){});
//
//        return responseEntity.getBody();
//    }

    public ResponseEntity<String> createUser(UserDTO user){
        var requestEntity = new HttpEntity<>(user, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/user/create", HttpMethod.POST, requestEntity, String.class);
    }

    public ResponseEntity<UserDTO> updateUser(UserDTO user){
        var requestEntity = new HttpEntity<>(user, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/user/update", HttpMethod.PUT, requestEntity, UserDTO.class);
    }

    private HttpHeaders createHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
