package eu.isilviu.tw.ResourceManagement.web.dto;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {

    private long id;

    private List<RoleDTO> roles = new ArrayList<>();

    private String username;

    private String firstName;

    private String lastName;

    private String hash;

    private Boolean isAdmin;

    public UserDTO() {
    }

    public UserDTO(String username, String firstName, String lastName, String hash, Boolean isAdmin){
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hash = hash;
        this.isAdmin = isAdmin;
    }

    public long getId() { return id; }

    public List<RoleDTO> getRoles() { return roles; }

    public String getUsername() { return username; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getHash() { return hash; }

    public Boolean isAdmin() { return isAdmin; }

    public void setAdmin(Boolean admin) { isAdmin = admin; }

    public void addRole(RoleDTO role){
        boolean alreadyContainsRole = roles.stream().anyMatch(roleDTO -> roleDTO.getTitle().equalsIgnoreCase(role.getTitle()));
        if(!alreadyContainsRole)
            roles.add(role);
    }

    public boolean removeRole(RoleDTO role){
        return roles.removeIf(roleDTO -> (roleDTO.getTitle().equalsIgnoreCase(role.getTitle())));
    }
}
