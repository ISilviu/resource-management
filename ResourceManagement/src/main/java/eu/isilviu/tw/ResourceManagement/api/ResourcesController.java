package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.Resource;
import eu.isilviu.tw.ResourceManagement.persistence.ResourcesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ResourcesController {

    @Autowired
    private ResourcesRepository resourcesRepository;

    @GetMapping(
            value = "/api/resources")
    public List<Resource> getResources(){
        return resourcesRepository.findAll();
    }

}
