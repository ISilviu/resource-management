package eu.isilviu.tw.ResourceManagement.persistence;

import eu.isilviu.tw.ResourceManagement.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    User getByUsername(@NotNull String username);

    boolean existsByUsername(@NotNull String username);

    List<User> getAllByIsAdmin(Boolean isAdmin);
}
