package eu.isilviu.tw.ResourceManagement.api;

import eu.isilviu.tw.ResourceManagement.models.resources.Holiday;
import eu.isilviu.tw.ResourceManagement.persistence.HolidaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HolidaysController {

    @Autowired
    private HolidaysRepository holidaysRepository;

    @PostMapping(value = "/api/holiday/create",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public Holiday createHoliday(@RequestBody Holiday holiday){
        return holidaysRepository.save(holiday);
    }

    @PutMapping(value = "/api/holiday/update",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Holiday updateHoliday(@RequestBody Holiday holiday){
        return holidaysRepository.save(holiday);
    }

    @DeleteMapping(value = "/api/holiday/delete",
                consumes =  MediaType.APPLICATION_JSON_VALUE)
    public void deleteHoliday(@RequestBody Holiday holiday){
        if(holiday == null)
            return;
        holidaysRepository.delete(holiday);
    }

    @GetMapping(value = "/api/holidays",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Holiday> getAll(){
        return holidaysRepository.findAll();
    }
}
