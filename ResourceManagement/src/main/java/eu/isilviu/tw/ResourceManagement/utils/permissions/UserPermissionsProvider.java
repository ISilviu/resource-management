package eu.isilviu.tw.ResourceManagement.utils.permissions;

import eu.isilviu.tw.ResourceManagement.services.UserRestService;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.RightDTO;
import eu.isilviu.tw.ResourceManagement.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserPermissionsProvider {

    @Autowired
    private UserRestService userRestService;

    private UserDTO currentUserInstance;

    public UserPermissionsProvider fetchNew(Long id){
        currentUserInstance = userRestService.getUserForId(id);
        return this;
    }

    public boolean hasRight(RightDTO requiredRight, ResourceDTO requiredResource){
        if(currentUserInstance == null)
            return false;

        var roles = currentUserInstance.getRoles();
        for(var role : roles){
            var rights = role.getRights();
            for(var right : rights){
                var resource = right.getResource();
                if(requiredResource.getTableName().equals(resource.getTableName())){
                    if(right.getRightType() == requiredRight.getRightType())
                        return true;
                }
            }
        }
        return false;
    }

}
