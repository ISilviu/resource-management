package eu.isilviu.tw.ResourceManagement;

import eu.isilviu.tw.ResourceManagement.models.Right;
import eu.isilviu.tw.ResourceManagement.models.RightType;
import eu.isilviu.tw.ResourceManagement.persistence.RightsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ResourceManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResourceManagementApplication.class, args);
	}

//	@Autowired
//	private RightsRepository rightsRepository;

	private static List<RightType> rightTypes = Arrays.asList(RightType.values());

//	@PostConstruct
//	private void initializeDefaultRights(){
//		rightTypes.stream().forEach(rightType -> {
//			if(!rightsRepository.existsByRightType(rightType))
//				rightsRepository.save(new Right(rightType));
//		});
//	}

}
