package eu.isilviu.tw.ResourceManagement.persistence;

import eu.isilviu.tw.ResourceManagement.models.resources.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentsRepository extends JpaRepository<Student, Long> {
}
