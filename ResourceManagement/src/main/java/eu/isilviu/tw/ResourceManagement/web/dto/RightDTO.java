package eu.isilviu.tw.ResourceManagement.web.dto;

import eu.isilviu.tw.ResourceManagement.models.RightType;

import java.util.Objects;

public class RightDTO {

    private long id;

    public void setResource(ResourceDTO resource) {
        this.resource = resource;
    }

    private ResourceDTO resource;

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    private RoleDTO role;

    private RightType rightType;

    public RightDTO() { }

    public RightDTO(RightType rightType){ this.rightType = rightType;}

    public RightDTO(RightType rightType, ResourceDTO resource) {
        this.rightType = rightType;
        this.resource = resource;
    }

    public RightDTO( RoleDTO role, RightType rightType) {
        this.role = role;
        this.rightType = rightType;
    }

    public RightDTO(ResourceDTO resource, RoleDTO role, RightType rightType) {
        this.resource = resource;
        this.role = role;
        this.rightType = rightType;
    }

    public long getId() { return id; }

    public ResourceDTO getResource() { return resource; }

    public RoleDTO getRole() { return role; }

    public RightType getRightType() { return rightType; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RightDTO rightDTO = (RightDTO) o;
        return rightType == rightDTO.rightType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rightType);
    }
}
