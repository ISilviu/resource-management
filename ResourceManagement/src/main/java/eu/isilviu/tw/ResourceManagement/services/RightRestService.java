package eu.isilviu.tw.ResourceManagement.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.isilviu.tw.ResourceManagement.web.dto.RightDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Component
public class RightRestService {

    private RestTemplate restTemplate;

    public RightRestService(){
        restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        var converter = new MappingJackson2HttpMessageConverter(mapper);
        restTemplate.getMessageConverters().add(converter);
    }

    public RightDTO createRight(RightDTO right){
        var requestEntity = new HttpEntity<>(right, createHeaders());
        return restTemplate.exchange(CommonLiterals.HOSTNAME + "/api/right/create", HttpMethod.POST, requestEntity, RightDTO.class)
                .getBody();
    }

    private HttpHeaders createHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
