package eu.isilviu.tw.ResourceManagement.persistence;

import eu.isilviu.tw.ResourceManagement.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.html.Option;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByTitle(@NotNull String title);

    boolean existsByTitle(@NotNull String title);
}
