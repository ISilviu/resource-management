package eu.isilviu.tw.ResourceManagement;

import eu.isilviu.tw.ResourceManagement.persistence.ResourcesRepository;
import eu.isilviu.tw.ResourceManagement.persistence.RolesRepository;
import eu.isilviu.tw.ResourceManagement.persistence.UsersRepository;
import eu.isilviu.tw.ResourceManagement.web.dto.ResourceDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;

@SpringBootTest
class ResourceManagementApplicationTests {

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private RolesRepository rolesRepository;

	@Autowired
	private ResourcesRepository resourcesRepository;

	private RestTemplate restTemplate;

	@Test
	void getResources(){
		restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		String uriString = UriComponentsBuilder.fromHttpUrl("http://localhost:8080" + "/api/resources")
				.build()
				.toUriString();
		ResponseEntity<List<ResourceDTO>> responseEntity = restTemplate.exchange(uriString, HttpMethod.GET,
				null, new ParameterizedTypeReference<>() {
				});
	}


	@Test
	void contextLoads() {
		var resources = resourcesRepository.findAll();
		for (var resource : resources){
			var y = resource.getRights();
		}
		var x = 2;
	}

	@Test
	@Transactional
	void checkIfRoleHasRights(){
		var role = rolesRepository.findByTitle("Spintecatorul");
		var x = role.get().getRights();
	}

	@Test
	@Transactional
	void testToGetResourceRights(){
		var resources = resourcesRepository.findAll();
		for(var res : resources){
			var rights = res.getRights();
			for(var right : rights){
				var x = 2;
			}
		}
	}


}
